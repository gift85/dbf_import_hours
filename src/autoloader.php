<?php
spl_autoload_register(function ($classname){
    $classname = strtolower($classname);
    $classname = str_replace('\\', '/', $classname);

    /*не отрабатывает !file_exists("$classname.php") почему то*/
    if(!include_once("$classname.php")){

        /*как использовать собственное исключение в загрузчике классов?*/
        throw new Exception("class $classname.php not found");
    }
});
