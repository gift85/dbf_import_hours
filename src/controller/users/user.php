<?php

namespace Controller\Users;

use Controller\Main;
use Model\System;

abstract class User extends Main{
    //protected $user_id;

    public function __construct(){
        //$this->user_id = System::auth_check();
        //var_dump($this->user_id);
    }

    public function render():string{
        $html = System::make_template('main.php', $this->page_vars);
        return $html;
    }
}
