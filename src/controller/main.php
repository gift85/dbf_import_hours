<?php

namespace Controller;

use Core\Exceptions\E404;

abstract class Main{
    public $params;
    protected $page_vars;

    public function load_params(array $params){
        $this->params = $params;
    }

    public abstract function render():string;

    public function __call($name, $params){
        throw new E404("Unknown action \"$name\"");
    }

    public function __toString(){
        return $this->render();
    }
}
