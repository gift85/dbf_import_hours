<?php

namespace Controller;

use Core\Exceptions\E404;
use Model\{Page, System, Users};
use Controller\Users\User;
use Core\Validation;
/*разделить на смысловые классы*/
class Hours extends User{

    public function action_index(){
        $page = Page::instance();
        $tabno = $_POST['tabno'] ?? '';
        $month = $_POST['month'] ?? date("m");
        $year = $_POST['year'] ?? date("Y");
        $date = date("Y-m-d", mktime(0, 0, 0, $month, 1, $year));
        //$year_month = $year . $month;
        $begin = new \DateTime($date);
        $begin->modify('last Monday of previous month');
        $end = new \DateTime($date);
        $end->modify('first Monday of next month');
        $interval = \DateInterval::createFromDateString('1 day');
        $days = new \DatePeriod($begin, $interval, $end);
        //var_dump($days);
        //var_dump(str_pad($month, 2, '0', STR_PAD_LEFT));
        //var_dump($month);
        //var_dump($year);
        if(!empty($tabno)){
            $hours = $page->get_hours($tabno, $year . $month);
            //var_dump($hours);
        }
        /**/

        if(!empty($_SESSION['msg'])){
            $message = $_SESSION['msg'];
            unset($_SESSION['msg']);
            $message_class = 'info';
        }
        else{
            $message = '';
            $message_class = '';
        }
        $menu[] = System::menu_item('hours/import', 'подгрузить часики');

        $inner_vars = compact('hours', 'days', 'month', 'year', 'tabno', 'menu');
        $page_content = System::make_template("hours.php", $inner_vars);

        $page_title = 'Часики';

        $this->page_vars = compact('menu', 'page_content', 'page_title');
    }

    public function action_import(){
        $dbf_root = 'F:\\BD_INK_COPY\\ink';

        //$path_to_base = '\\\\server2003\\u2\\ink';
        //var_dump($path_to_base);
        //echo file_exists($path_to_base . '\\nar\\DBF\\narn.dbf') ? 'true' : 'not';
        if(!file_exists('dbfs\\chas.dbf')){
            copy($dbf_root . '\\nar\\dbf\\chas.dbf', 'dbfs\\chas.dbf');
        }

        set_time_limit(1230);
        $config = [
            /*"db_host"         => "localhost",*/
            /*"db_port"         => 3306,*/
            /*"db_username"     => "root",*/
            "db_password"     => "",
            "db_name"         => "fox_nev",
            /*"db_charset"      => "utf8",*/
            "dbf_path"        => "dbfs\\",
            /*"dbf_list"        => null,*/
            /*"table_prefix"    => null,*/
            /*"columns_only"    => false,*/
            "deleted_records" => false,
            /*"key_field"       => null,*/
            "verbose"         => false,
            "log_path"        => realpath(dirname(__FILE__))."/dbf2mysql.log"
        ];
        new \Inok\Dbf2mysql\convert($config);

        unlink('dbfs\\chas.dbf');
        System::redirect_if_true(true);
    }

    public function action_show():void{

    }

    public function action_logout():void{
        System::redirect_if_true(true);
        if(isset($this->params[1])){
            if(isset($_SESSION['auth'])){
                $login = $_SESSION['auth'];
                unset($_SESSION['auth']);
                unset($_SESSION[$login]);
            }
            setcookie('login', '', time() - 3600);
        }
        System::redirect_if_true(true);
    }

    public function action_login():void{
        System::redirect_if_true(true);
        $message = 'Welcome';
        $message_class = 'info';

        if(count($_POST) > 0){
            $login = trim($_POST['login']);
            $password = trim($_POST['password']);
            $page = Users::instance();
            $user_data= $page->get_user($login);
            /*заменить на другую ошибку*/
            if(is_null($user_data)){
                throw new E404('User not found!');
            }
            /*errmsg 'User not found'*/

            if(md5($password) === $user_data['password']){
                $_SESSION['auth'] = $user_data['user_id'];
                if(isset($_POST['save_me'])){
                    $mark = md5(time() . md5($password));
                    $_SESSION[$user_data['user_id']] = $mark;
                    setcookie($user_data['user_id'], $mark, time() + 3600 * 24 * 7);
                    //setcookie('login', $login, time() + 3600 * 24 * 7);
                }
                else{
                    setcookie($login, '', time());
                }
                System::redirect_if_true(true);
            }
            else{
                $message = 'Access denied';
                $message_class = 'error';
            }
        }

        /*message for authorized users*/
        elseif($this->user_id){
            $message = "You are already authorized.<br> If you want to log out push <a href='" . ROOT . "blog/login/logout'>HERE</a>";
            $message_class = 'error';
        }

        $is_authorized = !empty($this->user_id);
        $inner_vars = compact('message_class', 'message', 'is_authorized');
        $page_content = System::make_template("login.php", $inner_vars);
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: Authorization';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_register():void{
        System::redirect_if_true(true);
    }
}






