<?php

namespace Core;

use Core\Traits\Singleton;
use PDO, PDOException;

class Sql{
    use Singleton;
    protected $db;

    /**
     * конструктор всегда не будет принимать параметров ибо это не настроено
     * connecting to mysql database
     * @param string $host
     * @param string $dbname
     * @param string $name
     * @param string $pass
     */
    protected function __construct(string $host = 'localhost', string $dbname = 'fox_nev', string $name = 'root', string $pass = ''){
        try{
            $this->db = new PDO("mysql:host=$host;dbname=$dbname", $name, $pass, [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NAMED,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                /*PDO::ATTR_PERSISTENT => true*/
            ]);
        }
        catch(PDOException $e){
            throw new \Exception("Something was wrong.<br> DB CONNECTION ERROR: {$e->getMessage()}");
        }
    }

    public function select(string $sql, array $params = []):array{
        $query = $this->execute_query($sql, $params);
        return $query->fetchAll(PDO::FETCH_GROUP);
    }

    public function insert(string $table, array $params):int{
        $keys = array_keys($params);
        $masks = array_map(function (string $key){
            return ":$key";
        }, $keys);

        $fields = implode(', ', $keys);
        $values = implode(', ', $masks);

        $sql = "INSERT INTO $table ($fields) VALUES ($values)";
        $query = $this->execute_query($sql, $params);
        return $query->rowCount();/*$this->db->lastInsertId();/*или rowCount()?*/
    }

    public function update(string $table, array $set, string $where, array $params = []):int{
        $keys = array_keys($set);
        $masks = array_map(function ($key){
            return "$key = :$key";
        }, $keys);
        $pairs = implode(', ', $masks);

        $merged_params = array_merge($set, $params);
        $sql = "UPDATE $table SET $pairs WHERE $where";
        $query = $this->execute_query($sql, $merged_params);
        return $query->rowCount();
    }

    public function delete(string $table, string $where, array $params = []):int{
        $sql = "DELETE FROM $table WHERE $where";
        $query = $this->execute_query($sql, $params);
        return $query->rowCount();
    }

    /***
     * execute query with simple error handling
     * @param string $sql
     * @param array $params
     * @return \PDOStatement
     */
    protected function execute_query(string $sql, array $params):\PDOStatement{
        $query = $this->db->prepare($sql);
        try{
            $query->execute($params);
        }
        catch(PDOException $e){
            throw new \Exception("ERROR {$e->getMessage()}");
            //echo 'ERROR ' . $e->getMessage(); /*temporary*/
            //exit(); /*temporary*/
        }
        return $query;
    }
}
