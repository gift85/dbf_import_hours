<?php
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 27.02.2017
 * Time: 12:25
 */

require_once('autoloader.php');

use Core\Exceptions\E404;

define('ROOT', '/');
define('CHAR_SET', 'UTF-8');/*not used yet*/
define('LOGS_PATH', 'src/logs/');
define('DEV_MODE', true);/*not used yet*/
setlocale(LC_ALL , 'ru_RU.UTF-8', 'Russian');/*адекватно не работает*/
$params = explode('/', $_GET['q']);

if(end($params) === ''){
    array_pop($params);
}

$param0 = $params[0] ?? 'hours';
$controllers = ['hours'];

if(!in_array($param0, $controllers)){
    $param0 = 'hours';
    $params[1] = '404';
}

$action = 'action_' . ($params[1] ?? 'index');

$controller_name = 'controller\\' . ucfirst($param0);

try{
    $controller = new $controller_name;
    $controller->load_params($params);
    $controller->$action();
    echo $controller;
}
catch(E404 $e){
    /*добавить отображение ошибки?*/
    echo 'smthwrng';
    $controller = new controller\Pages;
    //$controller->load_params($params);
    $controller->action_404();
    echo $controller;
}
catch(Exception $e){
    echo '<pre>';
    echo $e;
    echo '<pre>';
}
