<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?=$page_title?></title>
    <link rel="stylesheet" type="text/css" href="<?=ROOT?>src\css\style.css">
</head>
<body>

    <div id="wrapper">
        <div id="page_title">
            <?=$page_title ?? ''?>
        </div>

        <hr>
        <?=$page_content?>
    </div>
</body>
</html>
