<div class="message <?=$message_class ?? ''?>">
    <?=$message ?? ''?>
</div>
<div class="menu">
    <?foreach($menu as $item):?>
        <a href="<?=ROOT?><?=$item['href']?>"><?=$item['text']?></a>
    <?endforeach?>
</div>
<br>
<form method="post">
    <select name="month" class="month">
        <?for($i = 1; $i < 13; $i++):?>
            <option value="<?=str_pad($i, 2, '0', STR_PAD_LEFT)?>"<?if($i == $month) {echo ' selected';}?>>
                <?=iconv("cp1251", "UTF-8", strftime("%B", mktime(0, 0, 0, $i, 1, $year)))?>
            </option>
        <?endfor?>
    </select>
    <select name="year" class="year">
        <?for($i = 0; $i < 10; $i++):?>
            <option value="<?=date("Y") - $i?>"<?if(date("Y") - $i == $year) {echo ' selected';}?>>
                <?=date("Y") - $i?>
            </option>
        <?endfor?>
    </select>

    <label class="tabno">Табельный
    <input type="text" size="5" name="tabno" value="<?=$tabno ?? ''?>" class="tabno">
    </label>
    <button>Найти</button>
</form>
<hr>
<div class="content hours">
    <table>
        <tr>
            <?$week_time = 0;?>
            <?$total_time = 0;?>
            <?foreach($days as $day):?>
                <?
                $dow = $day->format("w");
                $index = $day->format("Y-m-d");
                $td_class = "day $index";

                if ($dow == 6 or $dow == 0){
                    $td_class .= " holiday";
                }
                if ($day->format("m") != $month){
                    $td_class .= " grey";
                }
                if(!empty($hours) and array_key_exists($index, $hours)){
                    $start = $hours[$index][0]['timevih'];
                    $end = $hours[$index][0]['timeuh'];

                    $d_start = strtotime($start);
                    $d_end = strtotime($end);

                    $d_diff = $d_end - $d_start;

                    $week_time += $d_diff;
                    $total_time += $d_diff;
                    //$perc = (int)(100 * $d_diff) / (3600 * 24);
                }?>
                <td class="<?=$td_class?>">
                    <div class="time">
                        <?if(!empty($hours) and array_key_exists($index, $hours)){
                            echo $start . '-';
                            echo $end . '<br>';
                        }?>
                    </div>
                    <?=$day->format("d")?>
                    <div class="time">
                        <?if(!empty($d_diff)):?>
                            <?=gmdate("H:i", $d_diff)?>
                            <?unset($d_diff)?>
                        <? endif ?>
                    </div>
                </td>
            <?if($dow == 0):?>
                <td class="day week">
                    <?=$day->format("W") . ' неделя года:'?>
                    <div class="time">
                        <?
                        $hs = (int) ($week_time / 3600);
                        $ms = ($week_time % 3600) / 60;
                        $ms = str_pad($ms, 2, '0', STR_PAD_LEFT)
                        ?>
                        <?=$hs . ':' . $ms?>
                    </div>
                </td>
                <?$week_time = 0;?>
                </tr><tr>
            <?endif?>
            <?endforeach?>
            <td class="day" colspan="8">
<!--                <div class="time">-->
                    <?
                    $hs = (int) ($total_time / 3600);
                    $ms = ($total_time % 3600) / 60;
                    $ms = str_pad($ms, 2, '0', STR_PAD_LEFT)
                    ?>
                    <?='Всего: ' . $hs . ' часов ' . $ms . ' минут'?>
<!--                </div>-->
            </td>
        </tr>
    </table>

</div>
