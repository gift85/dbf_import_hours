<?php

namespace Model;

use Core\Traits\Singleton;
use Core\Model;

class Page extends Model{
    use Singleton;

    protected function __construct(){
        parent::__construct();
        $this->table = 'articles';
        $this->primary_key = 'article_id';
    }

    public function validation_rules(){
        return [
            'fields' => ['tabn', 'data', 'timevih', 'timeob', 'timeuh', 'kol', 'pr', 'vidr', 'komment'],
            'not_empty' => ['tabn', 'data', 'timevih', 'timeuh'],
            'allow_html' => []
        ];
    }

    public function get_index():array{
        $sql = "SELECT article_id, title FROM articles ORDER BY created DESC";/*LIMIT :start, 10*/

        return $this->db->select($sql);
    }

    public function get_article(string $title_id):array{
        $sql = "SELECT title, content, created, name as user, user_id FROM articles LEFT JOIN users using(user_id) WHERE article_id = :title_id";
        $params = compact('title_id');

        return $this->db->select($sql, $params);
    }

    public function get_hours(string $tabno, string $y_month):array{
        $sql = "SELECT data, timevih, timeob, timeuh FROM chas WHERE tabn = :tabno and extract(YEAR_MONTH from data) = :y_month ORDER BY data";
        $params = compact('tabno', 'y_month');
        return $this->db->select($sql, $params);
    }
}
