<?php $start = microtime(true);

require 'vendor\autoload.php';

require_once("src/mainController.php");

$time = microtime(true) - $start;
printf('<br>Скрипт выполнялся %.10F сек.', $time);

function wsummation(int $n) {
    //your code
    $result = '0';
    if($n > 0){
        for ($i = 1; $i <= $n; $i++){
            $result .= "+$i";
        }
    }
    return $result;
}

var_dump(wsummation(8));

function wsummation2(int $n) {
    return $n <= 0 ? '0' : wsummation2($n - 1) . '+' . $n;
}

var_dump(wsummation2(8));

function summation(int $n) {
    //your code
    $result = 0;
    if($n > 0){
        for ($i = 1; $i <= $n; $i++){
            $result += $i;
        }
    }
    return $result;
}

var_dump(summation(8));

function summation2(int $n) {
    return $n <= 1 ? 1 : summation2($n - 1) + $n;
}

var_dump(summation2(8));

function summation3(int $n):int{
    return $n > 0 ? $n * ($n  + 1) / 2 : 0;
}

var_dump(summation3(8));

function pal($str){
    $str = mb_strtolower($str);
    $str = preg_replace('/[^a-zа-я0-9]/ui', '', $str);
    var_dump($str);
    preg_match_all('/./us', $str, $ar);
    $strrev = join('', array_reverse($ar[0]));
    var_dump($strrev);
    var_dump($strrev);
    return $str === $strrev;
}

var_dump(pal('ав. В.А'));
